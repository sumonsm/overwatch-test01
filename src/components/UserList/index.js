/* @flow */

import React from 'react';
import { Link } from 'react-router-dom';

import styles from './styles.scss';

type Props = { list: Array<Object> };

const UserList = ({ list }: Props) => (
  <div className={styles.UserList}>
    <h4>Overwatch Heroes</h4>
    <ul>
      {list.map(user => (
        <li key={user.id}>
          <Link to={`/UserInfo/${user.id}`}>{user.attributes.name}</Link>
        </li>
      ))}
    </ul>
  </div>
);

export default UserList;

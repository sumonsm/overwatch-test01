/* @flow */

import React from 'react';
import styles from './styles.scss';

type Props = { info: Object };

const UserCard = ({ info }: Props) => (
  <div className={styles.UserCard}>
    <br />
    <a href="/">Back to Home</a>
    <h4>Hero Info</h4>
    <ul>
      <li>Name: {info.name}</li>
      <li>slug: {info.slug}</li>
      <li>
        image_portrait:
        <img src={info.image_portrait} alt={info.name} />
      </li>
      <li>
        image_splash:
        <img src={info.image_splash} alt={info.name} height={200} />
      </li>
      <li>updated_at: {info.updated_at}</li>
      <li>
        image_card_background:
        <img src={info.image_card_background} alt={info.name} height={200} />
      </li>
    </ul>
  </div>
);

export default UserCard;

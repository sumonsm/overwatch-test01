module.exports = {
  host: process.env.NODE_HOST || 'localhost', // Define your host from 'package.json'
  port: process.env.PORT,
  app: {
    htmlAttributes: { lang: 'en' },
    title: 'Overwatch Heroes',
    titleTemplate: 'Overwatch Heroes - %s',
    meta: [
      {
        name: 'description',
        content: 'The cavalory s ere.'
      }
    ]
  }
};
